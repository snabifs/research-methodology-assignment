#!/bin/sh

gcc meanmode.c -o mean123
gcc rangeco.c -o range123 -lm
echo "Main program is running"
cat /dev/null > output.c
echo '\n' "Please enter the input filename : " 
read ipFilename

while true; do

echo '\n' "Please choose one from the menu " '\n' "A for Calculation of Mean Median Mode " '\n' "B for Calculation of Range, InterQuarter Range and Standard Deviation " '\n' "G for Displaying Graphs in libreoffice "'\n' "X for exit "
read readMenu

if  ( [ "$readMenu" = "A" ] || [ "$readMenu" = "B" ] )
then
	echo -n "Valid entries "
fi

case "$readMenu" in
  A)
    echo "Print Mean,Mode,Median"
    ./mean123 $ipFilename 0
    ;;

  B)
    echo "Print Range,Interquarter range ,Coefficient of variation,Standard deviation"
    ./range123 $ipFilename 0
    ;;
  G) 
   echo "Print Graph "
    cat /dev/null > output.c
   ./mean123 $ipFilename 4
   ./range123 $ipFilename 5
   libreoffice --calc /home/mtech/Desktop/Shabnam/shabu/CTGRAPH.ods
    ;;
  X)
    echo '\n' "Exit"
    break
    ;;
  *)
	echo '\n' "Invalid choice, please try again "
    ;;
esac

#if ( [ "$readOption" <> "X" ] && [ "$readOption" <> "A" ] && [ "$readOption" <> "B" ] )
#then
#	echo '\n' "Invalid entries, please try again "
#fi
done


Fathima Shabnam
