//Program to find Range,Interquartile Range,SD,CV
/***************************************************************
*******Program to find Range,Interquartile Range,SD,CV*******
****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define MAXSIZE 1000
FILE *fop;
int range_function(int[],int);
int iqr_function(int[],int);
float sd_function(int[],int);
float cv_function(int[],int);
void main(char argc, char **argv)
{
	FILE *fp;
	char buf[1000];
	char arr[100];
	char *value;
	char *inputval[MAXSIZE];
	int found=0;
	int i=0,j=0,a[MAXSIZE],n=0,k,temp,choice,range,iqr,u=0,v=0,w=0,m=0,rungraph=0;
	float sd,cv;

	fp = fopen(argv[1],"r");
	fop = fopen("output.c","a+");

	while (fgets(buf,1000, fp)!=NULL)
	{
		value = strtok(buf," ,:;\n\t");
		while (value != NULL)
		{
	    	inputval[i] = strdup(value);
	    	value = strtok (NULL, " ,:;\n\t");
	    	i++;
		}

	}

	n = i;
	for(k=0; k<n; k++)
	{
		a[k] = atof(inputval[k]);
	}

	//Sort the Input array
	for(i=0;i<n;i++)
	{
		for(j=i+1;j<n;j++)
		{
			if(a[i]>a[j])
			{
				temp=a[i];
				a[i]=a[j];
				a[j]=temp;
			}
		}
	}
if (strcmp(argv[2],"5") == 0)
	{	
		choice=5;
		rungraph=1;
	}

	do {
	if (strcmp(argv[2],"5") != 0)
	{
	printf("\n\t1.Range \n\t2.Interquartile range \n\t3.Standard Deviation \n\t4.Coefficient of Variation\n\t5. Write to File\n\t6.Exit\n\tEnter your choice: ");
	scanf("%d",&choice);
}
	switch(choice)
	{
		case 1 : range=range_function(a,n);
			 printf("\t\tRange = %d",range);
			 break;
		case 2 : iqr=iqr_function(a,n);
			 printf("\t\tInterquartile range = %d",iqr);
			 break;
		case 3 : sd=sd_function(a,n);
			 printf("\t\tStandard Deviation = %f",sd);
			 break;
		case 4 : cv=cv_function(a,n);
			 printf("\t\tCoefficient of variation = %f",cv);
			 break;
		case 5 :
		 	 range=range_function(a,n);
		 	 iqr=iqr_function(a,n);
		 	 sd=sd_function(a,n);
		 	 cv=cv_function(a,n);
				if(fop){
				fclose(fop);
				fop = fopen("output.c","a+");}
				while (fgets(arr,100, fop)!=NULL)
					{
				if((strstr(arr, "Range")) != NULL) {
				found=1;
				}
				}
				if(found==0)
				{
					 	 fprintf(fop,"Range = %d \n",range);
						 	 fprintf(fop,"IQR = %d \n",iqr);
						 	 fprintf(fop,"SD = %f \n",sd);
						 	 fprintf(fop,"CV = %f \n",cv);
				}if (rungraph==0)
								break;
							 else
								exit(0);


				/*while(fscanf(fop,"%d",&arr[u]) > 0)
				{
				//	 printf("%d\n",a[i]);
					 u++;
					 m++;
					 fprintf(fop,"size=%d",m);
				}
				//printf("\nArray with Unique list  : ");
  				 for (u= 0; u < m; u++) {
    			         	for (v = u + 1; v< m;) {
         			 		if (arr[u] == arr[v]) {
            			 			for (w = v; w < m; w++) {
               			 				arr[w] = arr[w + 1];
            			 						}
            							m--;
         							      } else
            							v++;
      							       }
   							}
 
			   for (u = 0; u < m; u++) {
			      fprintf(fop,"%d ", arr[u]);
			   }break;*/
		case 6:
		 	 exit(0);
		default :printf("\n Wrong Choice");
	}

}while(choice!=6);

}

//Range
int range_function(int a[],int n){
	int range=a[n-1]-a[0];
	return range;
}

//Interquartile Range
int iqr_function(int a[], int n)
{
	int total;
	if(n%2==0){
		total = n;
	}
	else{
		total = n+1;
	}
	int lower = (.25)*total;
	int upper = (.75)*total;
	float lower_value = (a[lower]+a[lower+1])/2;
	float upper_value = (a[upper]+a[upper+1])/2;
	float total_value = upper_value-lower_value;
	return total_value;
}

//Standard Deviation
float sd_function(int a[], int n){
	float avg,ch,var;
	float sd,b[50];
	int i;
	for(i=0;i<n;i++)
			 avg=avg+a[i];
			 avg=avg/n;
			 for(i=0;i<n;i++)
			 {
				ch=a[i]-avg;
				ch=ch*ch;
				b[i]=ch;
			 }
			 for(i=0;i<n;i++)
				var=var+b[i];
			 var=var/n;
			 sd=sqrt(var);
			return sd;

}

//Coefficient of Variation
float cv_function(int a[], int n)
{
	int i;
	float avg;
	float sd,cv;
	for(i=0;i<n;i++)
		avg=avg+a[i];
	avg=avg/n;
	sd= sd_function(a,n);
	cv=sd/avg;
	return cv;
}
Fathima Shabnam
