//Program to find Mean,Median,and Mode
/***************************************************************
******* Program to find Mean,Median,and Mode *******
****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXSIZE 1000
float mean_funct(float[],int);
float median_funct(float[],int);
float mode_funct(float[],int);
int main(char argc, char **argv)

{
	FILE *ptr_file;
	FILE *out_file;
	char buf[1000];
	char a[1000];
	char *value;
	char *inputval[MAXSIZE];
	int i=0,found=0,rungraph=0;
	int k=0;
	int n,choice;
	float array[MAXSIZE],mean,median,mode;

	ptr_file =fopen(argv[1],"r");
	if (!ptr_file)
		return 1;

	out_file = fopen("output.c","a+");
	if (!out_file)
		return 1;

	while (fgets(buf,1000, ptr_file)!=NULL)
	{
		value = strtok(buf," ,:;\n\t");
		while (value != NULL)
		{
	    	inputval[i] = strdup(value);
	    	value = strtok (NULL, " ,:;\n\t");
	    	i++;
		}

	}

	n = i;
	for(k=0; k<n; k++)
	{
		array[k] = atof(inputval[k]);
	}

	if (strcmp(argv[2],"4") == 0)
	{	
		choice=4;
		rungraph=1;
	}
	do
	{
	if (strcmp(argv[2],"4") != 0)
	{	
	printf("\n\tEnter Choice:\n\t1.Mean\n\t2.Median\n\t3.Mode\n\t4.Write to File\n\t5.Exit\n\t");
	scanf("%d",&choice);
	}
	switch(choice)
	{
	case 1:
		mean=mean_funct(array,n);
		printf("\n\tMean  %f\n",mean);
		break;
	case 2:
		median=median_funct(array,n);
		printf("\n\tMedian  %f\n",median);
		break;
	case 3:
		mode=mode_funct(array,n);
		printf("\n\tMode  %f\n",mode);
		break;
	case 4:
		if(out_file){
		fclose(out_file);
		out_file = fopen("output.c","a+");}
			mean=mean_funct(array,n);
			median=median_funct(array,n);
			mode=mode_funct(array,n);
		while (fgets(a,1000, out_file)!=NULL)
			{
		if((strstr(a, "Mean")) != NULL) {
		found=1;
		}
		}
		if(found==0)
		{
			fprintf(out_file,"Mean = %f\n",mean);
			fprintf(out_file,"Median = %f\n",median);
			fprintf(out_file,"Mode = %f\n",mode);
		}
		if (rungraph==0)
				break;
			else
				exit(0);
	case 5:
		break;
	default:
		printf("Wrong Option");
		break;
	}
	}while(choice!=5);

	fclose(ptr_file);
	fclose(out_file);
    return 0;
}


float mean_funct(float array[],int n)
{
int i;
float sum=0;
for(i=0;i<n;i++)
sum=sum+array[i];
return (sum/n);
}

float median_funct(float a[],int n)
{
float temp;
int i,j;
for(i=0;i<n;i++)
for(j=i+1;j<n;j++)
{
if(a[i]>a[j])
{
temp=a[j];
a[j]=a[i];
a[i]=temp;
}
}
if(n%2==0)
return (a[n/2]+a[n/2-1])/2;
else
return a[n/2];
}
/*float mode_function(float a[],int n)
{
return (3*median_function(a,n)-2*mean_function(a,n));
}
*/
float mode_funct(float a[],int n)
{

int  i, j,k, mod[1000]={0},count=0, t=0, f=0;

   
   for(i=0;i<n;i++){
        count=0;
        for(j=i+1;j<=n;j++){
            if(a[i]==a[j]){
                count++;
            }
        }
        if(t<count){
            t=count;
            mod[0]=a[i];
            mod[1]='\0';
        }
        else if(t == count){
            f=0;
            for(k=0;mod[k]!='\0';k++){
                if(a[i]==mod[k]){
                    f=1;
                }
            }
            if(f==0){
                for(k=0;mod[k]!='\0';k++);
                mod[k]=a[i];
                mod[++k]='\0';
            }
        }
    }
float great=mod[0];
for(i=1;mod[i]!='\0';i++)
{

if((mod[i]>great) && mod[i]!='\0')
{
great=mod[i];
}
}
return great; //fprintf(out_file,"\nMode = %d",great); 
   
    
}


Fathima Shabnam
